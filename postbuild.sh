#!/usr/bin/env bash
rm -rf ../public/static
rm -rf ../public/asset-manifest.json
rm -rf ../public/favicon.ico
rm -rf ../public/index.html
rm -rf ../public/manifest.json
rm -rf ../public/service-worker.js
mv ./build/* ../public/
