import { createFormAction } from 'redux-form-saga';

export const addProduct = createFormAction('vzr/product/ADD_PRODUCT');
export const saveProduct = createFormAction('vzr/product/SAVE_PRODUCT');
