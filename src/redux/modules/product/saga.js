import { put, call, takeLatest, getContext } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import { addProduct, saveProduct } from './actions';

function* handleSaveProduct(action) {
  const api = yield getContext('api');
  try {
    yield call(api.saveProduct, action.payload);
    yield put(saveProduct.success());
  } catch (e) {
    const formError = new SubmissionError(e);
    yield put(saveProduct.failure(formError));
  }
}

export default function* productSaga() {
  yield takeLatest(saveProduct.REQUEST, handleSaveProduct);
  yield takeLatest(addProduct.REQUEST, handleSaveProduct);
}
