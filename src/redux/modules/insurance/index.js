import _ from 'lodash';
import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  FETCH_COMPANIES_REQUEST,
  FETCH_COMPANIES_SUCCESS,
  FETCH_COMPANIES_FAILURE,
} from './constants';

const initialState = {
  isCompaniesLoading: false,
  isProductsLoading: false,
  companies: [],
  products: [],
  entities: {
    companies: {},
  },
};

export default function insurance(state = initialState, action) {
  switch (action.type) {
    case FETCH_COMPANIES_REQUEST:
      return {
        ...state,
        isCompaniesLoading: true,
      };
    case FETCH_COMPANIES_SUCCESS:
      return {
        ...state,
        isCompaniesLoading: false,
        companies: action.payload.result,
        entities: _.merge({}, state.entities, action.payload.entities),
      };
    case FETCH_COMPANIES_FAILURE:
      return {
        ...state,
        isCompaniesLoading: false,
      };
    case FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        isProductsLoading: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        isProductsLoading: false,
      };
    case FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        isProductsLoading: false,
      };
    default:
      return state;
  }
}

export * from './actions';
export * from './selectors';
