import { schema } from 'normalizr';

export const companySchema = new schema.Entity(
  'companies',
  {},
  {
    idAttribute: 'id',
  },
);

export const productSchema = new schema.Entity(
  'products',
  {},
  {
    idAttribute: 'id',
  },
);
