import {
  FETCH_COMPANIES_FAILURE,
  FETCH_COMPANIES_REQUEST,
  FETCH_COMPANIES_SUCCESS,
} from './constants';

export function fetchCompanies() {
  return {
    type: FETCH_COMPANIES_REQUEST,
  };
}

export function fetchCompaniesSuccess(payload) {
  return {
    type: FETCH_COMPANIES_SUCCESS,
    payload,
  };
}

export function fetchCompaniesFailure(e) {
  return {
    type: FETCH_COMPANIES_FAILURE,
    payload: e,
  };
}
