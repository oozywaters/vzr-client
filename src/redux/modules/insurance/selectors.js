import { createSelector } from 'reselect';

const companiesArraySelector = state => state.insurance.companies;
const companyEntitiesSelector = state => state.insurance.entities.companies;

export const companySelector = (state, id) =>
  state.insurance.entities.companies[id];
export const companiesSelector = createSelector(
  companiesArraySelector,
  companyEntitiesSelector,
  (companies, entities) => companies.map(id => entities[id]),
);

export const productsSelector = createSelector();
