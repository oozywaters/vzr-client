import { takeLatest, getContext, call, put } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import { companySchema } from './schemas';

import { FETCH_COMPANIES_REQUEST } from './constants';
import { fetchCompaniesSuccess, fetchCompaniesFailure } from './actions';

function* fetchCompanies() {
  const api = yield getContext('api');
  try {
    const response = yield call(api.fetchInsuranceCompanies);
    const normalizedData = normalize(response, [companySchema]);
    yield put(fetchCompaniesSuccess(normalizedData));
  } catch (e) {
    yield put(fetchCompaniesFailure(e));
  }
}

export default function* insuranceSaga() {
  yield takeLatest(FETCH_COMPANIES_REQUEST, fetchCompanies);
}
