import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import insurance from './insurance';
import acquiring from './acquiring';
import contract from './contract';

export default combineReducers({
  acquiring,
  insurance,
  contract,
  router: routerReducer,
  form: formReducer,
});
