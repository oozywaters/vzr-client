import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE,
} from './constants';

const initialState = {
  isLoading: false,
  transactions: Array(50)
    .fill()
    .map((item, index) => ({
      id: index + 1,
      fullName: 'Иванов Иван Иванович',
      telephone: '9243331511',
      email: 'mail@mail.com',
      purchaseDate: '2018-01-01',
      price: index + 100,
      contractStatus: 'Статус',
      paymentStatus: 'Оплачено',
      partner: 'АВТО.ру',
    })),
};

export default function acquiringReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TRANSACTIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}

export * from './actions';
