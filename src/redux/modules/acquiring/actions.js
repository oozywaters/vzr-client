import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE,
} from './constants';

export function fetchTransactions() {
  return {
    type: FETCH_TRANSACTIONS_REQUEST,
  };
}

export function fetchTransactionsSuccess(payload) {
  return {
    type: FETCH_TRANSACTIONS_SUCCESS,
    payload,
  };
}

export function fetchTransactionsFailure(e) {
  return {
    type: FETCH_TRANSACTIONS_FAILURE,
    payload: e,
  };
}
