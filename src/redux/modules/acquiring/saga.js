import { takeLatest, getContext, call, put } from 'redux-saga/effects';
import { FETCH_TRANSACTIONS_REQUEST } from './constants';
import { fetchTransactionsSuccess, fetchTransactionsFailure } from './actions';

function* fetchTransactions() {
  const api = yield getContext('api');
  try {
    const response = yield call(api.fetchTransactions);
    yield put(fetchTransactionsSuccess(response));
  } catch (e) {
    yield put(fetchTransactionsFailure(e));
  }
}

export default function* acquiringSaga() {
  yield takeLatest(FETCH_TRANSACTIONS_REQUEST, fetchTransactions);
}
