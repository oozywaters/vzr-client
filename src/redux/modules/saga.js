import { all, fork } from 'redux-saga/effects';
import formActionSaga from 'redux-form-saga';
import insuranceSaga from './insurance/saga';
import productSaga from './product/saga';
import acquiringSaga from './acquiring/saga';
import contractSaga from './contract/saga';

export default function* rootSaga() {
  yield all([
    // Sagas list
    fork(insuranceSaga),
    fork(productSaga),
    fork(acquiringSaga),
    fork(contractSaga),
    fork(formActionSaga),
  ]);
}
