import {
  FETCH_CONTRACT_REQUEST,
  FETCH_CONTRACT_SUCCESS,
  FETCH_CONTRACT_FAILURE,
  TERMINATE_CONTRACT_REQUEST,
  TERMINATE_CONTRACT_SUCCESS,
  TERMINATE_CONTRACT_FAILURE,
} from './constants';

export function fetchContract(id) {
  return {
    type: FETCH_CONTRACT_REQUEST,
    payload: { id },
  };
}

export function fetchContractSuccess(payload) {
  return {
    type: FETCH_CONTRACT_SUCCESS,
    payload,
  };
}

export function fetchContractFailure(e) {
  return {
    type: FETCH_CONTRACT_FAILURE,
    payload: e,
  };
}

export function terminateContract(contract) {
  return {
    type: TERMINATE_CONTRACT_REQUEST,
    payload: contract,
  };
}

export function terminateContractSuccess(payload) {
  return {
    type: TERMINATE_CONTRACT_SUCCESS,
    payload,
  };
}

export function terminateContractFailure(e) {
  return {
    type: TERMINATE_CONTRACT_FAILURE,
    payload: e,
  };
}
