import {
  FETCH_CONTRACT_REQUEST,
  FETCH_CONTRACT_SUCCESS,
  FETCH_CONTRACT_FAILURE,
  TERMINATE_CONTRACT_REQUEST,
  TERMINATE_CONTRACT_SUCCESS,
  TERMINATE_CONTRACT_FAILURE,
} from './constants';

const initialState = {
  isLoading: false,
};

export default function contractReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CONTRACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_CONTRACT_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case FETCH_CONTRACT_FAILURE:
      return {
        ...state,
        isLoading: false,
      };
    case TERMINATE_CONTRACT_REQUEST:
    case TERMINATE_CONTRACT_SUCCESS:
    case TERMINATE_CONTRACT_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
}

export * from './actions';
