import { call, put, takeLatest, getContext } from 'redux-saga/effects';
import {
  FETCH_CONTRACT_REQUEST,
  TERMINATE_CONTRACT_REQUEST,
} from './constants';
import {
  fetchContractSuccess,
  fetchContractFailure,
  terminateContractSuccess,
  terminateContractFailure,
} from './actions';

function* fetchContract(action) {
  const api = yield getContext('api');
  const { id } = action.payload;
  if (!id) {
    return;
  }
  try {
    const response = yield call(api.fetchContract, id);
    yield put(fetchContractSuccess(response));
  } catch (e) {
    yield put(fetchContractFailure(e));
  }
}

function* terminateContract(action) {
  const api = yield getContext('api');
  try {
    const response = yield call(api.terminateContract, action.payload);
    yield put(terminateContractSuccess(response));
  } catch (e) {
    yield put(terminateContractFailure(e));
  }
}

export default function* contractSaga() {
  yield takeLatest(FETCH_CONTRACT_REQUEST, fetchContract);
  yield takeLatest(TERMINATE_CONTRACT_REQUEST, terminateContract);
}
