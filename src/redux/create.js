import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import rootReducer from './modules/reducer';
import rootSaga from './modules/saga';
import { name, version } from '../../package.json';

export default function configureStore(history, client, initialState = {}) {
  const sagaMiddleware = createSagaMiddleware({
    context: {
      api: client,
    },
  });
  const middleware = [routerMiddleware(history), sagaMiddleware];

  let enhancer;

  if (process.env.NODE_ENV === 'development') {
    middleware.push(
      createLogger({
        collapsed: true,
      }),
    );

    // https://github.com/zalmoxisus/redux-devtools-extension#14-using-in-production
    const composeEnhancers = composeWithDevTools({
      // Options: https://github.com/zalmoxisus/redux-devtools-extension/blob/master/docs/API/Arguments.md#options
      name: `${name}@${version}`,
    });

    // https://redux.js.org/docs/api/applyMiddleware.html
    enhancer = composeEnhancers(applyMiddleware(...middleware));
  } else {
    enhancer = applyMiddleware(...middleware);
  }

  // https://redux.js.org/docs/api/createStore.html
  const store = createStore(rootReducer, initialState, enhancer);

  // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./modules/reducer', () =>
      // eslint-disable-next-line global-require
      store.replaceReducer(require('./modules/reducer').default),
    );
  }

  sagaMiddleware.run(rootSaga);

  return store;
}
