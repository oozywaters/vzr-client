import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import Button from 'react-bootstrap/lib/Button';
import RouteTitle from 'components/RouteTitle/RouteTitle';
import EndContractConfirmModal from 'components/EndContractConfirmModal/EndContractConfirmModal';

import { fetchContract, terminateContract } from 'redux/modules/contract';

class ContractDetails extends Component {
  static propTypes = {
    match: T.shape({
      url: T.string.isRequired,
      params: T.shape({
        id: T.string.isRequired,
      }).isRequired,
    }).isRequired,
    transaction: T.shape(),
    fetchContract: T.func.isRequired,
    terminateContract: T.func.isRequired,
  };

  static defaultProps = {
    transaction: null,
  };

  state = {
    isConfirmDialogOpen: false,
  };

  componentDidMount() {
    const { match } = this.props;
    this.props.fetchContract(match.params.id);
  }

  onModalConfirm = () => {
    this.closeConfirmDialog();
    this.props.terminateContract(this.props.transaction);
  };

  get invoice() {
    const { transaction } = this.props;
    if (!transaction) {
      return null;
    }
    return (
      <section className="invoice">
        <div className="row">
          <div className="col-xs-12">
            <h2 className="page-header">
              <i className="fa fa-address-card" /> Contract #{transaction.id}
              <small className="pull-right">
                Date: {transaction.purchaseDate}
              </small>
            </h2>
          </div>
        </div>
        <div className="row invoice-info">
          <div className="col-sm-12 invoice-col">
            <b>Insurant</b>
            <br />
            <br />
            <div>{transaction.fullName}</div>
            <div>
              <a href={`tel:${transaction.telephone}`}>
                {transaction.telephone}
              </a>
            </div>
            <div>
              <a href={`mailto:${transaction.email}`}>{transaction.email}</a>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-xs-12">
            <p className="text-muted well well-sm no-shadow">
              * Если кнопка неактивна, для расторжения договора необходимо {}
              связаться с представителями СК
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <Button
              className="pull-right"
              bsStyle="danger"
              onClick={this.openConfirmDialog}
            >
              <i className="fa fa-times-circle " /> {}
              <FormattedMessage id="transactions.details.terminate" />
            </Button>
            <Button
              className="pull-right"
              bsStyle="primary"
              style={{ marginRight: 5 }}
            >
              <i className="fa fa-undo " /> {}
              <FormattedMessage id="transactions.details.moneyBack" />
            </Button>
          </div>
        </div>
      </section>
    );
  }

  openConfirmDialog = () => this.setState({ isConfirmDialogOpen: true });

  closeConfirmDialog = () => this.setState({ isConfirmDialogOpen: false });

  render() {
    const { match } = this.props;
    return (
      <div>
        <RouteTitle
          title={<FormattedMessage id="transactions.details.title" />}
        />
        <BreadcrumbsItem to={match.url}>{match.params.id}</BreadcrumbsItem>
        {this.invoice}
        <div className="clearfix" />
        <EndContractConfirmModal
          show={this.state.isConfirmDialogOpen}
          onHide={this.closeConfirmDialog}
          onConfirm={this.onModalConfirm}
        />
      </div>
    );
  }
}

function mapStateToProps(state, { match }) {
  return {
    transaction: state.acquiring.transactions[match.params.id],
  };
}

export default connect(mapStateToProps, { fetchContract, terminateContract })(
  ContractDetails,
);
