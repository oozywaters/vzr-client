import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import ProductListTable from 'components/ProductListTable/ProductListTable';
import ProductCreateModal from 'components/ProductCreateModal/ProductCreateModal';
import RouteTitle from 'components/RouteTitle/RouteTitle';

import { companySelector } from 'redux/modules/insurance';

import s from './Products.module.css';

class Products extends Component {
  static propTypes = {
    company: T.shape({
      name: T.string.isRequired,
    }),
  };

  static defaultProps = {
    company: null,
  };

  state = {
    isModalOpen: false,
  };

  openModal = () => this.setState({ isModalOpen: true });

  closeModal = () => this.setState({ isModalOpen: false });

  render() {
    const { company } = this.props;
    if (!company) {
      return 'Company not found';
    }
    return (
      <section className="content">
        <RouteTitle title={company.name} />
        <button
          type="button"
          className="btn btn-primary btn-sm"
          onClick={this.openModal}
        >
          <i className="fa fa-plus" />{' '}
          <FormattedMessage id="insurance.products.add" />
        </button>
        <ProductListTable className={s.table} />
        <ProductCreateModal
          show={this.state.isModalOpen}
          onHide={this.closeModal}
        />
      </section>
    );
  }
}

function mapStateToProps(state, { match }) {
  return {
    company: companySelector(state, match.params.id),
  };
}

export default connect(mapStateToProps)(Products);
