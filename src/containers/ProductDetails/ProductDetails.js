import React, { Component } from 'react';
import ProductForm from 'components/ProductForm/ProductForm';
import RouteTitle from 'components/RouteTitle/RouteTitle';

export default class ProductDetails extends Component {
  render() {
    return (
      <section className="content">
        <RouteTitle title="Product Details" />
        <ProductForm />
      </section>
    );
  }
}
