import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ThroughProvider } from 'react-through';

import Header from 'components/Header';
import Footer from 'components/Footer';
import Sidebar from 'components/Sidebar';
import ControlSidebar from 'components/ControlSidebar';
import RoutesWithSubroutes from 'components/RoutesWithSubroutes/RoutesWithSubroutes';
import RouteHeader from 'components/RouteHeader/RouteHeader';

import { fetchCompanies, companiesSelector } from 'redux/modules/insurance';

class App extends Component {
  static propTypes = {
    routes: T.arrayOf(T.shape()).isRequired,
    companies: T.arrayOf(
      T.shape({
        name: T.string.isRequired,
        code: T.string.isRequired,
      }),
    ).isRequired,
    fetchCompanies: T.func.isRequired,
  };

  componentDidMount() {
    this.props.fetchCompanies();
  }

  get content() {
    return (
      <div className="content-wrapper">
        <RouteHeader />
        <RoutesWithSubroutes routes={this.props.routes} />
      </div>
    );
  }

  render() {
    return (
      <ThroughProvider>
        <div className="wrapper">
          <Header />
          <Sidebar companies={this.props.companies} />
          {this.content}
          <Footer />
          <ControlSidebar />
        </div>
      </ThroughProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    companies: companiesSelector(state),
  };
}

export default withRouter(
  connect(mapStateToProps, {
    fetchCompanies,
  })(App),
);
