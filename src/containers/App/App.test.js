/* eslint-env jest */
/* eslint-disable padded-blocks, no-unused-expressions */

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import routes from '../../routes';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App routes={routes} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
