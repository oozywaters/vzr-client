import React, { Component } from 'react';
import RouteTitle from 'components/RouteTitle/RouteTitle';

export default class Home extends Component {
  render() {
    return (
      <section className="content">
        <RouteTitle title="Home" description="Home Description" />
        Home component Here
      </section>
    );
  }
}
