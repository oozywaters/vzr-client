import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import TransactionListTable from 'components/TransactionListTable/TransactionListTable';
import RouteTitle from 'components/RouteTitle/RouteTitle';
import RoutesWithSubroutes from 'components/RoutesWithSubroutes/RoutesWithSubroutes';

import { fetchTransactions } from 'redux/modules/acquiring';

class Transactions extends Component {
  static propTypes = {
    transactions: T.arrayOf(
      T.shape({
        id: T.number.isRequired,
      }),
    ).isRequired,
    match: T.shape({
      url: T.string.isRequired,
    }).isRequired,
    fetchTransactions: T.func.isRequired,
    routes: T.arrayOf(T.shape()),
  };

  static defaultProps = {
    routes: [],
  };

  componentDidMount() {
    this.props.fetchTransactions();
  }

  get transactionsList() {
    return (
      <section className="content">
        <TransactionListTable transactions={this.props.transactions} />
      </section>
    );
  }

  render() {
    const { match } = this.props;
    return (
      <div>
        <RouteTitle title={<FormattedMessage id="transactions.title" />} />
        <BreadcrumbsItem to={match.url} icon={<i className="fa fa-exchange" />}>
          <FormattedMessage id="navigation.transactions" />
        </BreadcrumbsItem>
        <Route exact path={match.url} component={() => this.transactionsList} />
        <RoutesWithSubroutes routes={this.props.routes} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    transactions: state.acquiring.transactions,
  };
}

export default connect(mapStateToProps, { fetchTransactions })(Transactions);
