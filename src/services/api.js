import axios from 'axios';

const COMPANIES_LIST_URL = '/admin/api/company/list';

let client;

export default class ApiClient {
  constructor(options = {}) {
    client = axios.create(options);
  }

  saveProduct = data => {
    // eslint-disable-next-line
    console.log(data);
  };

  fetchInsuranceCompanies = () =>
    client.get(COMPANIES_LIST_URL).then(response => response.data.payload);

  fetchTransactions = () => {
    // eslint-disable-next-line
    console.log('Fetching Transactions...');
  };

  fetchContract = id => {
    // eslint-disable-next-line
    console.log(`Fetching Contract id: ${id}`);
  };

  terminateContract = contract => {
    // eslint-disable-next-line
    console.log('Terminating contract...', contract);
  };
}
