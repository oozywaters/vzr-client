import $ from 'jquery';
import './vendor.css';

window.jQuery = $;
require('admin-lte/dist/js/adminlte');
require('datatables/media/js/jquery.dataTables');
require('datatables.net-bs/js/dataTables.bootstrap');
