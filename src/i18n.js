import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ru from 'react-intl/locale-data/ru';
import flattenMessages from './utils/flattenMessages';

import enMessages from './translations/en-us.json';
import ruMessages from './translations/ru-ru.json';

addLocaleData([...en, ...ru]);

const translationMessages = {
  'en-US': flattenMessages(enMessages),
  'ru-RU': flattenMessages(ruMessages),
};

export default translationMessages;
