import React, { Component } from 'react';
import T from 'prop-types';
import { Breadcrumb } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Breadcrumbs as BreadcrumbsContainer } from 'react-breadcrumbs-dynamic';

class ItemWithIcon extends Component {
  static propTypes = {
    to: T.string,
    children: T.node.isRequired,
    icon: T.node,
    isFinal: T.bool,
  };

  static defaultProps = {
    to: null,
    icon: null,
    isFinal: false,
  };

  get content() {
    const { icon, children } = this.props;
    if (icon) {
      return (
        <span>
          {icon} {children}
        </span>
      );
    }
    return children;
  }

  render() {
    const { to, icon, children, isFinal, ...props } = this.props;
    if (isFinal) {
      return <li className="active">{this.content}</li>;
    }
    return (
      <LinkContainer exact to={to}>
        <Breadcrumb.Item {...props}>{this.content}</Breadcrumb.Item>
      </LinkContainer>
    );
  }
}

const Breadcrumbs = () => (
  <BreadcrumbsContainer
    container={Breadcrumb}
    item={ItemWithIcon}
    finalProps={{ isFinal: true }}
  />
);

export default Breadcrumbs;
