import React from 'react';
import { throughAgentFactory, throughContainer } from 'react-through';

const AREA = 'routeTitle';
const BEARING_KEY = 'default';

const RouteTitle = throughAgentFactory(AREA);

export const RouteTitleContainer = throughContainer(AREA)(props => {
  const data = props.routeTitle[BEARING_KEY];
  if (!data) {
    return null;
  }
  return (
    <h1>
      {data.title}
      <small>{data.description}</small>
    </h1>
  );
});

export default RouteTitle;
