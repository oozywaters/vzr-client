import React, { Component } from 'react';
import T from 'prop-types';
import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import { FormattedMessage } from 'react-intl';
import emptyFunction from 'fbjs/lib/emptyFunction';

class EndContractConfirmModal extends Component {
  static propTypes = {
    show: T.bool,
    onHide: T.func,
    onConfirm: T.func.isRequired,
  };

  static defaultProps = {
    show: false,
    onHide: emptyFunction,
  };

  render() {
    const { show, onHide, onConfirm } = this.props;
    return (
      <Modal className="modal-danger" show={show} onHide={onHide}>
        <Header closeButton>
          <Title>
            <FormattedMessage id="transactions.details.confirm.title" />
          </Title>
        </Header>
        <Body>
          <p>
            <FormattedMessage id="transactions.details.confirm.body" />
          </p>
        </Body>
        <Footer>
          <Button bsStyle="" className="btn-outline" onClick={onHide}>
            <FormattedMessage id="transactions.details.confirm.no" />
          </Button>
          <Button bsStyle="" className="btn-outline" onClick={onConfirm}>
            <FormattedMessage id="transactions.details.confirm.yes" />
          </Button>
        </Footer>
      </Modal>
    );
  }
}

export default EndContractConfirmModal;
