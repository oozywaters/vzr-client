import React, { Component } from 'react';
import T from 'prop-types';
import { Route, Switch } from 'react-router-dom';

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
const RouteWithSubRoutes = route => (
  <Route
    path={route.path}
    render={props => {
      const RouteComponent = route.main;
      return <RouteComponent {...props} routes={route.routes} />;
    }}
  />
);

export default class RoutesWithSubroutes extends Component {
  static propTypes = {
    routes: T.arrayOf(
      T.shape({
        path: T.string.isRequired,
      }),
    ).isRequired,
  };

  render() {
    return (
      <Switch>
        {this.props.routes.map(route => (
          <RouteWithSubRoutes key={route.path} {...route} />
        ))}
      </Switch>
    );
  }
}
