import $ from 'jquery';
import React, { Component } from 'react';
import T from 'prop-types';
import { FormattedMessage } from 'react-intl';
import cx from 'classnames';
import { Link } from 'react-router-dom';

function renderTableHeader() {
  return (
    <tr>
      <th>
        <FormattedMessage id="transactions.table.id" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.fullName" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.email" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.telephone" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.date" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.price" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.contractStatus" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.paymentStatus" />
      </th>
      <th>
        <FormattedMessage id="transactions.table.partner" />
      </th>
    </tr>
  );
}

class TransactionListTable extends Component {
  static propTypes = {
    className: T.string,
    transactions: T.arrayOf(
      T.shape({
        id: T.number.isRequired,
      }),
    ).isRequired,
  };

  static defaultProps = {
    className: null,
  };

  componentDidMount() {
    this.dataTableInstance = $(this.table).DataTable({
      pageLength: 50,
    });
  }

  componentWillUnmount() {
    if (this.dataTableInstance) {
      this.dataTableInstance.destroy();
    }
  }

  get tableBody() {
    const { transactions } = this.props;
    return (
      <tbody>
        {transactions.map(item => {
          const url = `/transactions/${item.id}`;
          return (
            <tr key={item.id}>
              <td>
                <Link to={url}>{item.id}</Link>
              </td>
              <td>{item.fullName}</td>
              <td>{item.email}</td>
              <td>{item.telephone}</td>
              <td>{item.purchaseDate}</td>
              <td>{item.price}</td>
              <td>{item.contractStatus}</td>
              <td>{item.paymentStatus}</td>
              <td>{item.partner}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }

  render() {
    const { className } = this.props;
    const classNames = cx('box box-primary', className);
    return (
      <div className={classNames}>
        <div className="box-header">
          <h3 className="box-title">
            <FormattedMessage id="transactions.table.title" />
          </h3>
        </div>
        <div className="box-body">
          <table
            ref={c => {
              this.table = c;
            }}
            className="table table-bordered table-hover"
          >
            <thead>{renderTableHeader()}</thead>
            {this.tableBody}
            <tfoot>{renderTableHeader()}</tfoot>
          </table>
        </div>
      </div>
    );
  }
}

export default TransactionListTable;
