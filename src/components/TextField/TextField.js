import React, { Component } from 'react';
import T from 'prop-types';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';

export default class TextField extends Component {
  static propTypes = {
    id: T.string,
    input: T.shape().isRequired,
    label: T.shape().isRequired,
    meta: T.shape({
      touched: T.bool.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    id: null,
  };

  get validationState() {
    const { meta: { touched, error } } = this.props;
    if (touched && error) {
      return 'error';
    }
    return null;
  }

  get helpBlock() {
    const { meta: { touched, error } } = this.props;
    if (touched && error) {
      return <HelpBlock>{error}</HelpBlock>;
    }
    return null;
  }

  render() {
    const { id, label, input } = this.props;
    return (
      <FormGroup controlId={id} validationState={this.validationState}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...input} />
        {this.helpBlock}
      </FormGroup>
    );
  }
}
