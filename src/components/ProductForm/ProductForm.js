import React, { Component } from 'react';
import T from 'prop-types';
import cx from 'classnames';
import emptyFunction from 'fbjs/lib/emptyFunction';
import { reduxForm, Field } from 'redux-form';
import { FormattedMessage } from 'react-intl';

import TextField from 'components/TextField/TextField';
import CheckboxField from 'components/CheckboxField/CheckboxField';

import { saveProduct } from 'redux/modules/product';

class ProductForm extends Component {
  static propTypes = {
    className: T.string,
    handleSubmit: T.func,
  };

  static defaultProps = {
    className: null,
    handleSubmit: emptyFunction,
  };

  render() {
    const { className, handleSubmit } = this.props;
    const classNames = cx('box box-primary', className);
    return (
      <div className={classNames}>
        <div className="box-header with-border">
          <h3 className="box-title">
            <FormattedMessage id="insurance.products.form.title" />
          </h3>
        </div>
        <form onSubmit={handleSubmit(saveProduct)}>
          <div className="box-body">
            <Field
              id="name"
              name="name"
              label={<FormattedMessage id="insurance.products.form.name" />}
              component={TextField}
            />
            <div className="form-group">
              <label htmlFor="limit">
                <FormattedMessage id="insurance.products.form.limit" />
              </label>
              <select className="form-control" name="limit" id="limit">
                <option value="1">40000</option>
                <option value="2">50000</option>
                <option value="3">60000</option>
                <option value="4">70000</option>
              </select>
            </div>
            <Field
              id="active"
              name="active"
              label={<FormattedMessage id="insurance.products.form.active" />}
              component={CheckboxField}
            />
          </div>
          <div className="box-footer">
            <button type="submit" className="btn btn-primary">
              <FormattedMessage id="insurance.products.form.save" />
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'productDetails',
})(ProductForm);
