import React, { Component } from 'react';
import T from 'prop-types';
import cx from 'classnames';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import { Checkbox } from 'react-icheck';
import s from './CheckboxField.module.css';

export default class CheckboxField extends Component {
  static propTypes = {
    id: T.string,
    input: T.shape().isRequired,
    label: T.shape().isRequired,
    meta: T.shape({
      touched: T.bool.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    id: null,
  };

  render() {
    const { id, label, input } = this.props;
    const checkboxClass = cx('icheckbox_minimal-blue', s.checkbox);
    return (
      <FormGroup controlId={id}>
        <Checkbox
          {...input}
          checkboxClass={checkboxClass}
          increaseArea="20%"
          label={label}
          checked={input.value}
        />
      </FormGroup>
    );
  }
}
