import React, { Component } from 'react';
import T from 'prop-types';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';

export default class MultipleSelectField extends Component {
  static propTypes = {
    id: T.string,
    label: T.shape().isRequired,
    input: T.shape().isRequired,
  };

  static defaultProps = {
    id: null,
  };

  render() {
    const { id, label, input } = this.props;
    return (
      <FormGroup controlId={id}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl componentClass="select" multiple {...input}>
          <option value="1">30 000</option>
          <option value="2">40 000</option>
          <option value="3">50 000</option>
          <option value="4">60 000</option>
          <option value="5">70 000</option>
        </FormControl>
      </FormGroup>
    );
  }
}
