import React, { Component } from 'react';
import T from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { LinkContainer } from 'react-router-bootstrap';
import NavItem from 'react-bootstrap/lib/NavItem';

export default class Sidebar extends Component {
  static propTypes = {
    companies: T.arrayOf(
      T.shape({
        id: T.number.isRequired,
        code: T.string.isRequired,
      }),
    ).isRequired,
  };

  get companies() {
    const { companies } = this.props;
    return (
      <ul className="treeview-menu">
        {companies.map(company => {
          const url = `/insurance/${company.id}`;
          return (
            <LinkContainer to={url} key={company.id}>
              <NavItem>
                <i className="fa fa-circle-o" />
                {company.name}
              </NavItem>
            </LinkContainer>
          );
        })}
      </ul>
    );
  }

  render() {
    return (
      <aside className="main-sidebar">
        <section className="sidebar">
          <div className="user-panel">
            <div className="pull-left image">
              <img
                src="https://img13.androidappsapk.co/300/3/c/1/ru.dedkowboy.JustFunGames.png"
                className="img-circle"
                alt="Ivan Petrov"
              />
            </div>
            <div className="pull-left info">
              <p>Ivan Petrov</p>
              <a href="/">
                <i className="fa fa-circle text-success" /> Online
              </a>
            </div>
          </div>

          <form action="#" method="get" className="sidebar-form">
            <div className="input-group">
              <input
                type="text"
                name="q"
                className="form-control"
                placeholder="Search..."
              />
              <span className="input-group-btn">
                <button
                  type="submit"
                  name="search"
                  id="search-btn"
                  className="btn btn-flat"
                >
                  <i className="fa fa-search" />
                </button>
              </span>
            </div>
          </form>

          <ul className="sidebar-menu" data-widget="tree">
            <li className="header">HEADER</li>
            <li className="treeview">
              <a href="/">
                <i className="fa fa-link" />
                {} <FormattedMessage id="sidebar.insurance.products" />
                <span className="pull-right-container">
                  <i className="fa fa-angle-left pull-right" />
                </span>
              </a>
              {this.companies}
            </li>
            <LinkContainer to="/transactions">
              <NavItem>
                <i className="fa fa-link" />{' '}
                <FormattedMessage id="sidebar.acquiring" />
              </NavItem>
            </LinkContainer>
          </ul>
        </section>
      </aside>
    );
  }
}
