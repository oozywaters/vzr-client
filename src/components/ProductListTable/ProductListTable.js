import React, { Component } from 'react';
import T from 'prop-types';
import { FormattedMessage } from 'react-intl';
import cx from 'classnames';
import { Checkbox } from 'react-icheck';
import { Link } from 'react-router-dom';

class ProductListTable extends Component {
  /* eslint-disable */
  static propTypes = {
    className: T.string,
  };

  static defaultProps = {
    className: null,
  };

  get tableBody() {
    return (
      <tr>
        <td>
          <Checkbox checkboxClass="icheckbox_square-blue" cursor="true" />
        </td>
        <td>
          <Link to="/products/1">2435</Link>
        </td>
        <td>
          Программа страхования "А"
        </td>
        <td>
          60000
        </td>
      </tr>
    );
  }

  render() {
    const classNames = cx('box box-primary', this.props.className);
    return (
      <div className={classNames}>
        <div className="box-header">
          <h3 className="box-title">
            <FormattedMessage id="insurance.products.title" />
          </h3>
        </div>
        <div className="box-body">
          <table className="table table-hover">
            <tbody>
              <tr>
                <th>
                  <FormattedMessage id="insurance.table.active" />
                </th>
                <th>
                  <FormattedMessage id="insurance.table.id" />
                </th>
                <th>
                  <FormattedMessage id="insurance.table.title" />
                </th>
                <th>
                  <FormattedMessage id="insurance.table.limit" />
                </th>
              </tr>
              {this.tableBody}
              {this.tableBody}
              {this.tableBody}
              {this.tableBody}
            </tbody>
          </table>
        </div>
        <div className="box-footer clearfix">
          <ul className="pagination pagination-sm no-margin pull-right">
            <li><a href="/">«</a></li>
            <li><a href="/">1</a></li>
            <li><a href="/">2</a></li>
            <li><a href="/">3</a></li>
            <li><a href="/">»</a></li>
          </ul>
        </div>
      </div>
    );
  }
}

export default ProductListTable;
