export default values => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Необходимо название';
  }
  return errors;
};
