import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import emptyFunction from 'fbjs/lib/emptyFunction';

import Modal, { Header, Title, Body, Footer } from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

import TextField from 'components/TextField/TextField';
import CheckboxField from 'components/CheckboxField/CheckboxField';
import MultipleSelectField from 'components/MultipleSelectField/MultipleSelectField';

import { addProduct } from 'redux/modules/product';

import validate from './validate';

class ProductCreateModal extends Component {
  static propTypes = {
    handleSubmit: T.func.isRequired,
    show: T.bool,
    onHide: T.func,
  };

  static defaultProps = {
    show: false,
    onHide: emptyFunction,
  };

  render() {
    const { handleSubmit, show, onHide } = this.props;
    return (
      <Modal show={show} onHide={onHide}>
        <Header closeButton>
          <Title>
            <FormattedMessage id="insurance.products.add" />
          </Title>
        </Header>
        <form onSubmit={handleSubmit(addProduct)}>
          <Body>
            <Field
              id="name"
              name="name"
              label={<FormattedMessage id="insurance.products.form.name" />}
              component={TextField}
            />
            <Field
              id="limit"
              name="limit"
              label={<FormattedMessage id="insurance.products.form.limit" />}
              component={MultipleSelectField}
            />
            <Field
              id="active"
              name="active"
              label={<FormattedMessage id="insurance.products.form.active" />}
              component={CheckboxField}
            />
          </Body>
          <Footer>
            <Button type="submit" bsStyle="primary">
              <FormattedMessage id="insurance.products.form.save" />
            </Button>
          </Footer>
        </form>
      </Modal>
    );
  }
}

function mapStateToProps() {
  return {
    initialValues: {
      name: 'Some Name',
      limit: [1, 3],
      active: true,
    },
  };
}

export default connect(mapStateToProps)(
  reduxForm({
    form: 'productAdd',
    validate,
  })(ProductCreateModal),
);
