import React, { Component } from 'react';
import { RouteTitleContainer } from 'components/RouteTitle/RouteTitle';
import Breadcrumbs from 'components/Breadcrumbs/Breadcrumbs';

export default class RouteHeader extends Component {
  render() {
    return (
      <section className="content-header">
        <RouteTitleContainer />
        <Breadcrumbs />
      </section>
    );
  }
}
