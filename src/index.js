import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import './vendor';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import routes from './routes';
import translationMessages from './i18n';
import createStore from './redux/create';
import ApiClient from './services/api';

const locale =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage ||
  'en-US';

const client = new ApiClient({
  baseURL: process.env.REACT_APP_API_BASE_URL,
});
const history = createHistory();
const store = createStore(history, client);

const container = document.getElementById('root');
const AppComponent = (
  <Provider store={store}>
    <IntlProvider locale={locale} messages={translationMessages[locale]}>
      <ConnectedRouter history={history}>
        <App routes={routes} />
      </ConnectedRouter>
    </IntlProvider>
  </Provider>
);

ReactDOM.render(AppComponent, container);
registerServiceWorker();
