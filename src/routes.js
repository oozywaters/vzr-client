import loadable from 'loadable-components';
import Home from './containers/Home';
import ProductDetails from './containers/ProductDetails/ProductDetails';
import ContractDetails from './containers/ContractDetails/ContractDetails';

const Products = loadable(() =>
  import(/* webpackChunkName: 'products' */ './containers/Products/Products'),
);

const Transactions = loadable(() =>
  import(/* webpackChunkName: 'transactions' */ './containers/Transactions/Transactions'),
);

const routes = [
  {
    path: '/',
    exact: true,
    main: Home,
  },
  {
    path: '/transactions',
    main: Transactions,
    routes: [
      {
        path: '/transactions/:id',
        exact: true,
        main: ContractDetails,
      },
    ],
  },
  {
    path: '/insurance/:id',
    exact: true,
    main: Products,
  },
  {
    path: '/products/:id',
    exact: true,
    main: ProductDetails,
  },
];

export default routes;
